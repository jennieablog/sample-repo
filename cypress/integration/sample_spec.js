describe( 'Home Page Nav Links', () => {
    it('has "home" link', () => {
        cy.visit('/')
        cy.contains('a','Home').click()
        cy.url().should('include','/index.html')
    })
    it('has "about" link', () => {
        cy.visit('/')
        cy.contains('a','About').click()
        cy.url().should('include','/about.html')
    })
    it('has "store" link', () => {
        cy.visit('/')
        cy.contains('a','Store').click()
        cy.url().should('include','/store.html')
    })
})